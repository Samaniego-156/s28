// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

// [SECTION] Routing Components
	const route = express.Router();

// [SECTION] Tasks Routes
	// Create task
	route.post('/', (req, res) => {
		let taskInfo = req.body;
		controller.createTask(taskInfo).then(result => {
			res.send(result)
		})
	});

	// Retrieve All Tasks
	route.get('/', (req, res) => {
		controller.getAllTasks().then(result => {
			res.send(result);
		});
	});

	// Retrieve Task
	route.get('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.getTask(taskId).then(outcome => {
			res.send(outcome);
		});
	});
	// Update Task
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let newStatus = req.body;
		controller.updateTask(id, newStatus).then(outcome => {
			res.send(outcome);
		});
	});
	// Delete Task
	route.delete('/:id', (req, res) => {
		let taskId = req.params.id
		controller.deleteTask(taskId).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Expose Route System
	module.exports = route;
