// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./routes/tasks');
	const userRoutes = require('./routes/users');

// [SECTION] Environment Variables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING;

// [SECTION] Database Connection
 	mongoose.connect(secret);
	
	let db = mongoose.connection;
	db.on("error", console.error.bind(console, 'Connection Error'));
	db.once("open", () => console.log('Now Connected to MongoDB Atlas'));

// [SECTION] Server Setup
	const server = express();
	const port = process.env.PORT;
	server.use(express.json());

// [SECTION] Routes
	server.use('/tasks', taskRoutes);
	server.use('/users', userRoutes);	

// [SECTION] Server Response
	server.listen(port, () => {
		console.log(`Server is running on port ${port}`);
	});
	server.get('/', (req, res) => {
		res.send(`Welcome to the App!`)
	}) 