// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const userBlueprint = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, 'First name is Required!']
		},
		lastName: {
			type: String,
			required: [true, 'Last name is Required!']
		},
		email: {
			type: String,
			required: [true, 'Email is Required!']
		},
		password: {
			type: String,
			required: [true, 'Password is Required!']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		tasks: [
			{
				taskId: {
					type: String,
					required: [true, 'Task Id is required!']
				},
				assignedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});

// [SECTION] Expose Model
	module.exports = mongoose.model("User", userBlueprint);