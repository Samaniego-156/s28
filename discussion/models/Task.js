// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const taskBlueprint = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Task Name is Required']
		},
		status: {
			type: String,
			default: 'pending'
		}
	});

// [SECTION] Expose Model
	module.exports = mongoose.model("Task", taskBlueprint);
