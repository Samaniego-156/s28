// [SECTION] Dependencies and Modules
	const Task = require('../models/Task');

// [SECTION] Functionalities

	// Create New Task
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name;
		let newTask = new Task({
			name: taskName
		});
		return newTask.save().then((task, error) => {
			if (error) {
				return 'Saving new task failed....';
			} else {
				return 'A new task was created!';
			}
		})
	};

	// Retrieve All Tasks
	module.exports.getAllTasks = () =>{
		return Task.find({}).then(searchResult => {
			return searchResult;
		})
	};

	// Retrieve Task
	module.exports.getTask = (data) => {

		return Task.findById(data).then(result => {
			return result;
		})
	};

	// Update Task
	module.exports.updateTask = (taskId, newTask) => {
		let newStatus = newTask.status;
		return Task.findById(taskId).then((foundTask, error) => {
			if (foundTask) {
				foundTask.status = newStatus;
				return foundTask.save().then((updatedTask, saveErr) => {
					if (saveErr) {
						return false;
					} else {
						return updatedTask;
					}
				})
			} else {
				return 'No task found!';
			}
		})
	};
	// Delete Task
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
			if (removedTask) {
				return `Task was deleted successfully!`;
			} else {
				return 'No accounts were removed!';
			}
		})
	};